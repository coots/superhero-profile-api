FROM python:3.9-rc-alpine3.12
MAINTAINER bondeveloper

ENV PYTHONUNBUFFERED 1

COPY ./requirements.txt /requirements.txt
RUN pip install -r /requirements.txt

RUN mkdir /app
COPY ./app /app
WORKDIR /app

RUN adduser -D bondev
USER bondev
